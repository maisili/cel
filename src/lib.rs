#![crate_name = "cel"]
#![crate_type = "rlib"]

#![allow(non_snake_case)]

use {
  std::collections::HashMap,
  async_std::{ fs::File, },
  serde::{ Deserialize, Serialize },
};

#[derive(Serialize, Deserialize)]
pub struct Cel {
  pub datoFiyld: DatoFiyld,
  pub iuniksFiyld: IuniksFiyld,
  pub du: Du,
}

#[derive(Serialize, Deserialize)]
pub struct DatoFiyld (HashMap<String, Datom>);

#[derive(Serialize, Deserialize)]
pub struct IuniksFiyld (HashMap<String, Iuniks>);

#[derive(Serialize, Deserialize)]
pub enum Iuniks {
  Eksek(String),
  Othyr(String),
}

#[derive(Serialize, Deserialize)]
pub enum Datom {
  DatoFiyld(String),
  Bool(bool),
  Int(usize),
  String(String),
  Yrei(Yrei),
  Lamdy(Lamdy),
}

#[derive(Serialize, Deserialize)]
pub enum Yrei {
  String(Vec<String>),
  Int(Vec<usize>),
}

#[derive(Serialize, Deserialize)]
pub enum Lamdy {
  If(If),
  Map(Map),
}

#[derive(Serialize, Deserialize)]
pub struct If {
  pub kyndicyn: Kyndicyn,
  pub then: Box<Datom>,
  pub els: Box<Datom>,
}

#[derive(Serialize, Deserialize)]
pub struct Map {
  pub yrei: Box<Datom>,
  pub datoFiyld: DatoFiyld,
  pub rityrn: Box<Datom>,
}

#[derive(Serialize, Deserialize)]
pub struct Kyndicyn {
  pub sobdjbek: Box<Datom>,
  pub ikualiti: Ikualiti,
  pub obdjek: Box<Datom>,
}

#[derive(Serialize, Deserialize)]
pub enum Ikualiti {
  Ikuyl,
  Greityr,
  Lesyr,
  IkuylOrGreityr,
  IkuylOrLesyr,
}

#[derive(Serialize, Deserialize)]
pub enum Du {
  Siskol(Siskol),
  Sikuyns(Vec<Du>),
  Eisink(Vec<Du>),
  If(IfDu),
  Breik,
  Kyntiniu,
  Stop,
}

#[derive(Serialize, Deserialize)]
pub struct IfDu {
  pub kyndicyn: Kyndicyn,
  pub then: Box<Du>,
  pub els: Box<Du>,
}

#[derive(Serialize, Deserialize)]
pub struct Siskol {
  pub eksykiutybyl: String,
  pub yrgiumynts: Datom,
  pub ynvairynmynt: DatoFiyld,
}
